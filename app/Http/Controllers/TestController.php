<?php

namespace App\Http\Controllers;

class TestController extends Controller
{
    private function init()
    {
        define('LIB_IGFS', __DIR__ . '/../../Libraries/IGFS_CG_API');
        require_once(LIB_IGFS . '/init/IgfsCgInit.php');

        $init               = new \IgfsCgInit();
        $init->serverURL    = env('UNICREDIT_SERVER_URL', 'https://testeps.netswgroup.it/UNI_CG_SERVICES/services');
        $init->timeout      = 30000;
        $init->shopID       = md5(random_bytes(32));
        $init->shopUserRef  = 'test@test.io';
        $init->trType       = 'PURCHASE';
        $init->currencyCode = 'EUR';
        $init->amount       = 10050;
        $init->langID       = 'IT';
        $init->notifyURL    = 'https://test.io';
        $init->errorURL     = 'https://test.io';
        $init->disableCheckSSLCert();

        return $init;
    }

    /**
     * Initiates a credit card payment in Unicredit
     * @result No failure
     */
    public function cc()
    {
        $init = $this->init();

        $init->tid  = env('UNICREDIT_CC_TID', 'UNI_ECOM');
        $init->kSig = env('UNICREDIT_CC_KSIG', 'UNI_TESTKEY');

        $result = $init->execute();
        dump('Execute result: ' . $result);
        dd($init);
    }


    /**
     * Initiates a credit card payment in Unicredit with a '_'(underscore) in the ShopID.
     * @result No failure
     */
    public function ccNoFailure()
    {
        $init = $this->init();

        $init->tid     = env('UNICREDIT_CC_TID', 'UNI_ECOM');
        $init->kSig    = env('UNICREDIT_CC_KSIG', 'UNI_TESTKEY');
        $init->shopID .= '_';

        $result = $init->execute();
        dump('Execute result: ' . $result);
        dd($init);
    }

    /**
     * Initiates a mybank payment in Unicredit
     * @result No failure
     */
    public function mybank()
    {
        $init = $this->init();

        $init->tid  = env('UNICREDIT_MYBANK_TID', 'UNI_MYBK');
        $init->kSig = env('UNICREDIT_MYBANK_KSIG', 'UNI_TESTKEY');

        $result = $init->execute();
        dump('Execute result: ' . $result);
        dd($init);
    }

    /**
     * Initiates a mybank payment in Unicredit with a '_'(underscore) in the ShopID.
     * @result Failure
     */
    public function mybankFailure()
    {
        $init = $this->init();

        $init->tid     = env('UNICREDIT_MYBANK_TID', 'UNI_MYBK');
        $init->kSig    = env('UNICREDIT_MYBANK_KSIG', 'UNI_TESTKEY');
        $init->shopID .= '_';

        $result = $init->execute();
        dump('Execute result: ' . $result);
        dd($init);
    }
}

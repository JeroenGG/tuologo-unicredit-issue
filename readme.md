<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Unicredit MyBank ShopID issue
Tested with: `PHP 7.3.9`

#### Setup
Open up a terminal in the root config of this repository.
1. `composer install`
2. `cp .env.example .env`
3. `php artisan serve` - Test URLs will now be available on `localhost:8000`

#### Test URLs (Routes)
Routes are available in the routes file (`routes/web.php`)
1. `localhost:8000/cc` - Initiates a credit card payment
2. `localhost:8000/cc/nofailure` - Initiates a credit card payment with an '_'(underscore) in the ShopID
3. `localhost:8000/mybank` - Initiates a mybank payment
4. `localhost:8000/mybank/failure` = Initiates a mybank payment with an '_'(underscore) in the ShopID

#### Test Code
The test code is available in the TestController(`app/Http/Controllers/TestController.php`).

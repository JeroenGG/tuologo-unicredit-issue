<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cc', 'TestController@cc');
Route::get('/cc/nofailure', 'TestController@cc');
Route::get('/mybank', 'TestController@mybank');
Route::get('/mybank/failure', 'TestController@mybankFailure');
